package util_test

import (
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig/internal/util"
)

func TestSplitCamelCaseByWords(t *testing.T) {
	t.Parallel()

	testCase := func(in string, want []string) {
		got := util.SplitCamelCaseByWords(in)
		if !reflect.DeepEqual(want, got) {
			t.Fatalf("unexpected result: want %#v, got %#v\n\tin: %q", want, got, in)
		}
	}

	testCase("", []string{})
	testCase("lowercase", []string{"lowercase"})
	testCase("Class", []string{"Class"})
	testCase("MyClass", []string{"My", "Class"})
	testCase("MyC", []string{"My", "C"})
	testCase("HTML", []string{"HTML"})
	testCase("PDFLoader", []string{"PDF", "Loader"})
	testCase("AString", []string{"A", "String"})
	testCase("SimpleXMLParser", []string{"Simple", "XML", "Parser"})
	testCase("vimRPCPlugin", []string{"vim", "RPC", "Plugin"})
	testCase("GL11Version", []string{"GL", "11", "Version"})
	testCase("99Bottles", []string{"99", "Bottles"})
	testCase("May5", []string{"May", "5"})
	testCase("BFG9000", []string{"BFG", "9000"})
	testCase("BöseÜberraschung", []string{"Böse", "Überraschung"})
	testCase("Two  spaces", []string{"Two", "  ", "spaces"})
	testCase("BadUTF8\xe2\xe2\xa1", []string{"BadUTF8\xe2\xe2\xa1"})
}
