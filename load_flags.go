package fconfig

import (
	"flag"
	"fmt"
	"reflect"
	"strconv"
	"time"

	"gitlab.com/so_literate/fconfig/internal/model"
	"gitlab.com/so_literate/fconfig/internal/util"
)

func (l *Loader) getFlagName(field *model.Field) string {
	if flagName, ok := field.GetTag(TagNameFlag); ok {
		return flagName
	}

	return field.GetName(l.Config.PrefixFlag, l.Config.DelimiterFlagWords, l.Config.DelimiterFlag, model.CaseNameModLower)
}

func setFlagBool(fs *flag.FlagSet, flagName, defaultValue, usage string) error {
	if defaultValue == "" {
		defaultValue = "false"
	}

	value, err := strconv.ParseBool(defaultValue)
	if err != nil {
		return fmt.Errorf("strconv.ParseBool: %w", err)
	}

	fs.Bool(flagName, value, usage)
	return nil
}

func setFlagString(fs *flag.FlagSet, flagName, defaultValue, usage string) {
	fs.String(flagName, defaultValue, usage)
}

func setFlagInt(fs *flag.FlagSet, tp reflect.Type, flagName, defaultValue, usage string) error {
	if defaultValue == "" {
		defaultValue = "0"
	}

	value, err := strconv.ParseInt(defaultValue, 0, tp.Bits())
	if err != nil {
		return fmt.Errorf("strconv.ParseBool: %w", err)
	}

	fs.Int(flagName, int(value), usage)
	return nil
}

func setFlagInt64(fs *flag.FlagSet, tp reflect.Type, flagName, defaultValue, usage string) error {
	if tp == util.DurationType {
		if defaultValue == "" {
			defaultValue = "0s"
		}

		value, err := time.ParseDuration(defaultValue)
		if err != nil {
			return fmt.Errorf("time.ParseDuration: %w", err)
		}

		fs.Duration(flagName, value, usage)
		return nil
	}

	if defaultValue == "" {
		defaultValue = "0"
	}

	value, err := strconv.ParseInt(defaultValue, 0, tp.Bits())
	if err != nil {
		return fmt.Errorf("strconv.ParseInt: %w", err)
	}

	fs.Int64(flagName, value, usage)
	return nil
}

func setFlagUint64(fs *flag.FlagSet, tp reflect.Type, flagName, defaultValue, usage string) error {
	if defaultValue == "" {
		defaultValue = "0"
	}

	value, err := strconv.ParseUint(defaultValue, 0, tp.Bits())
	if err != nil {
		return fmt.Errorf("strconv.ParseUint: %w", err)
	}

	fs.Uint64(flagName, value, usage)
	return nil
}

func setFlagFloat(fs *flag.FlagSet, tp reflect.Type, flagName, defaultValue, usage string) error {
	if defaultValue == "" {
		defaultValue = "0"
	}

	value, err := strconv.ParseFloat(defaultValue, tp.Bits())
	if err != nil {
		return fmt.Errorf("strconv.ParseFloat: %w", err)
	}

	fs.Float64(flagName, value, usage)
	return nil
}

func fixUsageEnumTags(usage, enum string) string {
	switch {
	case usage == "" && enum != "":
		return fmt.Sprintf("allowed: %s", enum)
	case usage != "" && enum != "":
		return fmt.Sprintf("%s (allowed: %s)", usage, enum)
	}

	return usage
}

func (l *Loader) setFlag(field *model.Field) error {
	flagName := l.getFlagName(field)
	defaultValue, _ := field.GetTag(TagDefault)
	usage, _ := field.GetTag(TagUsage)
	enum, _ := field.GetTag(TagEnum)

	usage = fixUsageEnumTags(usage, enum)

	kind := field.Type.Kind()

	// nolint:exhaustive // no need in every kinds here supported only.
	switch kind {
	case reflect.Bool:
		return setFlagBool(l.Config.Flags, flagName, defaultValue, usage)

	case reflect.String:
		setFlagString(l.Config.Flags, flagName, defaultValue, usage)
		return nil

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32:
		return setFlagInt(l.Config.Flags, field.Type, flagName, defaultValue, usage)

	case reflect.Int64: // maybe time.Duration
		return setFlagInt64(l.Config.Flags, field.Type, flagName, defaultValue, usage)

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return setFlagUint64(l.Config.Flags, field.Type, flagName, defaultValue, usage)

	case reflect.Float32, reflect.Float64:
		return setFlagFloat(l.Config.Flags, field.Type, flagName, defaultValue, usage)

	case reflect.Slice: // slices just string in flag
		setFlagString(l.Config.Flags, flagName, defaultValue, usage)
		return nil
	}

	return fmt.Errorf("%w: %s", ErrUnsupportedFieldType, kind.String())
}

// setFlags sets fields to flag set.
func (l *Loader) setFlags() error {
	if l.Config.SkipFlags {
		return nil
	}

	if l.Config.FileFlagConfig != "" {
		l.fileFlagConfig = l.Config.Flags.String(l.Config.FileFlagConfig, "", "path to config file")
	}

	for _, field := range l.fields {
		err := l.setFlag(field)
		if err != nil {
			return fmt.Errorf("failed to set flag of field: %s: %w", field.String(), err)
		}
	}

	return nil
}

// parseFlags just parse flags without loading.
func (l *Loader) parseFlags() error {
	if l.Config.Flags.Parsed() || l.Config.SkipFlags {
		return nil
	}

	return l.Config.Flags.Parse(l.Config.FlagArgs) // nolint:wrapcheck // no need in additional context.
}

func (l *Loader) loadFlags() {
	if l.Config.SkipFlags {
		return
	}

	flags := make(map[string]string, len(l.Config.FlagArgs))

	l.Config.Flags.Visit(func(f *flag.Flag) {
		flags[f.Name] = f.Value.String()
	})

	for _, field := range l.fields {
		flagName := l.getFlagName(field)

		value, ok := flags[flagName]
		if !ok { // field is not seted in args.
			continue
		}

		util.SetFieldValue(field, value) // nolint:errcheck,gosec // no need to check error, flag package already checked.
	}
}
