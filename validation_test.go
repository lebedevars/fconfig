package fconfig_test

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig"
)

func TestCheckFields(t *testing.T) {
	t.Parallel()

	type config struct {
		Int    int      `default:"5" enum:"1,3,5,7"`
		String string   `default:"value" enum:"text,value"`
		Slice  []int    `default:"1,2" enum:"[1 2],[3 4]"`
		SliceS []string `default:"text,second" enum:"[text second],[three]"`
		Int64  int64    `default:"1" required:"true"`
		Empty  int      `required:"false"`
	}

	got := config{}

	loader := fconfig.New(&got).SkipEnv().SkipFiles().SkipFlags()

	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{
		Int:    5,
		String: "value",
		Slice:  []int{1, 2},
		SliceS: []string{"text", "second"},
		Int64:  1,
	}

	if !reflect.DeepEqual(got, want) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	type outOfEnum struct {
		Int int `default:"2" enum:"1,3,5,7"`
	}

	loader.Destination = &outOfEnum{}
	err = loader.Load()
	if !errors.Is(err, fconfig.ErrNotEnumValue) {
		t.Fatal(err)
	}

	type emptyField struct {
		Int int `required:"true"`
	}

	loader.Destination = &emptyField{}
	err = loader.Load()
	if !errors.Is(err, fconfig.ErrEmptyRequiredField) {
		t.Fatal(err)
	}

	type emptyFieldWithoutTag struct {
		Int int
	}

	loader.Destination = &emptyFieldWithoutTag{}
	err = loader.AllFieldsRequired().Load()
	if !errors.Is(err, fconfig.ErrEmptyRequiredField) {
		t.Fatal(err)
	}
}
