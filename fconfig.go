// Package fconfig full config library to load configuration from several sources.
package fconfig

import (
	"flag"
	"fmt"
	"io"

	"gitlab.com/so_literate/fconfig/decoder/json"
	"gitlab.com/so_literate/fconfig/internal/model"
)

// FileDecoder describes decoder of the config file.
type FileDecoder interface {
	// Formats returns extensions of the supported files.
	Formats() []string
	// DecodeFile reads and decodes file to config destination.
	DecodeFile(path string, dst interface{}) error
	// Marshal encodes v into format with default values.
	Marshal(v interface{}) ([]byte, error)
}

// Config configuration of the Loader.
// Contains rules and parameters to load user configuration.
// By default loader will load values from:
// default tag, files (JSON by default), environment variables, flags.
type Config struct {
	// Will not load values from "default" tag.
	SkipDefaults bool
	// Will not load values from config files.
	SkipFiles bool
	// Will not load values from environment variables.
	SkipEnv bool
	// Will not load values from flags.
	SkipFlags bool

	// Prefix of the environment variable names (APP_ENV_NAME, APP_ENV_NAME_2).
	PrefixEnv string
	// Prefix if the flag names (app.flag_name, app.flag_name_2).
	PrefixFlag string

	// Delimiter of the environment variable names in the struct tree.
	// ('_' by default: Parent.ChildName -> PARENT_CHILDNAME).
	DelimiterEnv string
	// Delimiter of the environment variable names in the field name words.
	// ('_' by default: Parent.ChildName -> PARENTCHILD_NAME).
	DelimiterEnvWords string
	// Delimiter of the flag names in the struct tree.
	// ('.' by default: Parent.ChildName -> parent.childname).
	DelimiterFlag string
	// Delimiter of the flag names in the field name words.
	// ('_' by default: Parent.ChildName -> parentchild_name).
	DelimiterFlagWords string

	// List of the files with config values.
	// Values of the files will merge from first to the last file.
	Files []string
	// List of the config file decoders.
	FileDecoders map[string]FileDecoder
	// Returns error when one of the config files not found (Files).
	FileNotFoundAlert bool
	// Will merge config values even if they are not set in the file.
	FileForceMerge bool
	// It's a name of the flag to load file config using flag arg.
	// For example: FileFlagConfig: "config", then app -config=path/to/config.json.
	FileFlagConfig string

	// Will not return error on unknown environment variables.
	// By default loader will return error with not empty PrefixEnv.
	AllowUnknownEnvs bool
	// All config fields will be required,
	// loader will check every field after loaded all enabled sources.
	// Also you can mark field with tag "required".
	AllFieldsRequired bool

	// Will not os.Exit(0) if you pass -h/-help flag in args.
	FlagContinueOnHelp bool
	// os.Agrs just for test or you can pass your own arguments.
	FlagArgs []string
	// Flag set to defind flags.
	Flags *flag.FlagSet

	// Writer to write help messages and config examples.
	Output io.Writer
}

func setDefaultConfigValues(conf *Config) {
	if conf.DelimiterEnvWords == "" {
		conf.DelimiterEnvWords = "_"
	}

	if conf.DelimiterEnv == "" {
		conf.DelimiterEnv = "_"
	}

	if conf.DelimiterFlagWords == "" {
		conf.DelimiterFlagWords = "_"
	}

	if conf.DelimiterFlag == "" {
		conf.DelimiterFlag = "."
	}

	if conf.FileDecoders == nil {
		conf.FileDecoders = make(map[string]FileDecoder)
	}

	for _, format := range json.Decoder.Formats() {
		if _, ok := conf.FileDecoders[format]; !ok {
			conf.FileDecoders[format] = json.Decoder
		}
	}
}

// Loader is a config loader.
// It contains methods to set sources and parameters to load configuration.
type Loader struct {
	Destination interface{}
	Config      *Config

	cleanDestination interface{}
	fields           []*model.Field
	fileFlagConfig   *string
}

// New takes destination of the loader and optional config.
// dst must be a pointer and not nil value.
func New(dst interface{}, conf ...*Config) *Loader {
	l := &Loader{
		Destination: dst,
	}

	if len(conf) == 0 {
		l.Config = new(Config)
	} else {
		l.Config = conf[0]
	}

	setDefaultConfigValues(l.Config)

	return l
}

// Load loads config sources to destination config.
func (l *Loader) Load() error {
	if err := l.init(); err != nil {
		return fmt.Errorf("failed to init loader: %w", err)
	}

	if err := l.load(); err != nil {
		return fmt.Errorf("failed to load config: %w", err)
	}

	return nil
}

// SkipDefaults sets to config true.
// Will not load values from "default" tag.
func (l *Loader) SkipDefaults() *Loader {
	l.Config.SkipDefaults = true
	return l
}

// SkipFiles sets to config true.
// Will not load values from config files.
func (l *Loader) SkipFiles() *Loader {
	l.Config.SkipFiles = true
	return l
}

// SkipEnv sets to config true.
// Will not load values from environment variables.
func (l *Loader) SkipEnv() *Loader {
	l.Config.SkipEnv = true
	return l
}

// SkipFlags sets to config true.
// Will not load values from flags.
func (l *Loader) SkipFlags() *Loader {
	l.Config.SkipFlags = true
	return l
}

// PrefixEnv sets prefix to environment variable names (APP_ENV_NAME, APP_ENV_NAME_2).
func (l *Loader) PrefixEnv(prefix string) *Loader {
	l.Config.PrefixEnv = prefix
	return l
}

// PrefixFlag sets prefix to falg names (app.flag_name, app.flag_name_2).
func (l *Loader) PrefixFlag(prefix string) *Loader {
	l.Config.PrefixFlag = prefix
	return l
}

// DelimiterEnv sets delimiter of the environment variable names in the struct tree.
// ('_' by default: Parent.ChildName -> PARENT_CHILDNAME).
func (l *Loader) DelimiterEnv(delimiter string) *Loader {
	l.Config.DelimiterEnv = delimiter
	return l
}

// DelimiterEnvWords sets delimiter of the environment variable names in the field name.
// ('_' by default: Parent.ChildName -> PARENTCHILD_NAME).
func (l *Loader) DelimiterEnvWords(delimiter string) *Loader {
	l.Config.DelimiterEnvWords = delimiter
	return l
}

// DelimiterFlag sets delimiter of the flag names in the struct tree.
// ('.' by default: Parent.ChildName -> parent.childname).
func (l *Loader) DelimiterFlag(delimiter string) *Loader {
	l.Config.DelimiterFlag = delimiter
	return l
}

// DelimiterFlagWords sets delimiter of the flag names in the field name.
// ('_' by default: Parent.ChildName -> parentchild_name).
func (l *Loader) DelimiterFlagWords(delimiter string) *Loader {
	l.Config.DelimiterFlagWords = delimiter
	return l
}

// AddFile adds files to configuration of the loader.
func (l *Loader) AddFile(path string) *Loader {
	l.Config.Files = append(l.Config.Files, path)
	return l
}

// AddFileDecoder adds file decoder to configuration of the loader.
func (l *Loader) AddFileDecoder(fd FileDecoder) *Loader {
	for _, format := range fd.Formats() {
		l.Config.FileDecoders[format] = fd
	}
	return l
}

// FileNotFoundAlert sets to config true.
// Returns error when one of the config files not found (AddFile).
func (l *Loader) FileNotFoundAlert() *Loader {
	l.Config.FileNotFoundAlert = true
	return l
}

// FileForceMerge sets to config true.
// Will merge config values even if they are not installed in the file.
func (l *Loader) FileForceMerge() *Loader {
	l.Config.FileForceMerge = true
	return l
}

// FileFlagConfig sets a name of the flag to load file config using flag arg.
// For example: FileFlagConfig: "config", then run 'app -config=path/to/config.json'.
func (l *Loader) FileFlagConfig(flagName string) *Loader {
	l.Config.FileFlagConfig = flagName
	return l
}

// AllowUnknownEnvs sets to config true.
// Will not return error on unknown environment variables.
// By default loader will return error with not empty PrefixEnv.
func (l *Loader) AllowUnknownEnvs() *Loader {
	l.Config.AllowUnknownEnvs = true
	return l
}

// AllFieldsRequired sets to config true.
// All config fields will be required,
// loader will check every field after loaded all enabled sources.
// Also you can mark field with tag "required".
func (l *Loader) AllFieldsRequired() *Loader {
	l.Config.AllFieldsRequired = true
	return l
}

// FlagContinueOnHelp sets to config true.
// Will not os.Exit(0) if you pass -h/-help flag in args.
// Just return error after parsing.
func (l *Loader) FlagContinueOnHelp() *Loader {
	l.Config.FlagContinueOnHelp = true
	return l
}

// SetOutput sets output to write flag help message and examples of the config files.
func (l *Loader) SetOutput(w io.Writer) *Loader {
	l.Config.Output = w
	return l
}
