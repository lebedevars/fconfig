package fconfig_test

import (
	"errors"
	"os"
	"reflect"
	"strconv"
	"testing"

	"gitlab.com/so_literate/fconfig"
)

type brokenDecoder struct{}

func (*brokenDecoder) Formats() []string {
	return []string{".txt"}
}

var errBrokenDecoder = errors.New("broken decoder")

func (*brokenDecoder) DecodeFile(path string, dst interface{}) error {
	return errBrokenDecoder
}

func (*brokenDecoder) Marshal(v interface{}) ([]byte, error) {
	return nil, nil
}

func TestLoadFiles(t *testing.T) {
	t.Parallel()

	type config struct {
		Int    int `json:"tagged"`
		String string
		Slice  []int64
		Struct struct {
			TaggedField string `json:"tagged_field"`
		}
		Empty int
	}
	got := config{}

	loader := fconfig.New(&got).SkipDefaults().SkipEnv().SkipFlags().
		AddFile("./testdata/loader_test_load_files.json").
		AddFile("./testdata/not_exists_foo.bar")

	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{
		Int:    5,
		String: "text",
		Slice:  []int64{1, 2},
	}
	want.Struct.TaggedField = "tagged_field"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	err = loader.AddFile("./testdata/loader_test_load_files.txt").Load()
	if !errors.Is(err, fconfig.ErrDecoderNotFound) {
		t.Fatal(err)
	}

	err = loader.AddFileDecoder(&brokenDecoder{}).Load()
	if !errors.Is(err, errBrokenDecoder) {
		t.Fatal(err)
	}

	err = loader.FileNotFoundAlert().Load()
	if os.IsNotExist(err) {
		t.Fatal(err)
	}
}

func TestLoadFiles_ForceMerge(t *testing.T) {
	t.Parallel()

	type config struct {
		ExistInFirst  int
		ExistInSecond int
		ExistInBoth   int
		NoExist       int
	}

	got := config{NoExist: 44}

	loader := fconfig.New(&got).SkipDefaults().SkipEnv().SkipFlags().
		AddFile("./testdata/loader_test_load_files_merge_1.json").
		AddFile("./testdata/loader_test_load_files_merge_2.json")

	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{
		ExistInFirst:  1,  // From first file.
		ExistInSecond: 2,  // From second file.
		ExistInBoth:   22, // From second file.
		NoExist:       44, // From code in init.
	}

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	err = loader.FileForceMerge().Load() // Enable force merge.
	if err != nil {
		t.Fatal(err)
	}

	want = config{
		ExistInFirst:  0,  // From second file.
		ExistInSecond: 2,  // From second file.
		ExistInBoth:   22, // From second file.
		NoExist:       0,  // From second file.
	}

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}
}

func TestLoadFilesWithFlag(t *testing.T) {
	t.Parallel()

	type config struct {
		String string
	}
	got := config{}

	loader := fconfig.New(&got).SkipDefaults().SkipEnv().FileFlagConfig("config")
	loader.Config.FlagArgs = []string{`-config=./testdata/loader_test_load_files.json`}

	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{
		String: "text",
	}

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}
}

type dotEnvDecoder struct {
	decodeErr error
	values    map[string]string
}

func (*dotEnvDecoder) Formats() []string {
	return []string{".env"}
}

func (d *dotEnvDecoder) DecodeFile(path string, dst interface{}) error {
	values := dst.(*map[string]string) // nolint:errcheck,forcetypeassert // no need to check conversion.

	*values = d.values

	return d.decodeErr
}

func (*dotEnvDecoder) Marshal(v interface{}) ([]byte, error) {
	return nil, nil
}

func TestLoadFilesDotEnv(t *testing.T) {
	t.Parallel()

	type config struct {
		Int    int
		String string
		Slice  []int64
		Struct struct {
			SubField string
		}
		Empty int
	}

	got := config{}
	dec := &dotEnvDecoder{
		values: map[string]string{
			"INT":              "5",
			"STRING":           "text",
			"SLICE":            "1,2",
			"STRUCT_SUB_FIELD": "sub-value",
		},
	}

	loader := fconfig.New(&got).SkipDefaults().SkipEnv().SkipFlags().
		AddFile("./testdata/just_file.env").AddFileDecoder(dec)

	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{
		Int:    5,
		String: "text",
		Slice:  []int64{1, 2},
	}
	want.Struct.SubField = "sub-value"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	// decode error
	dec.decodeErr = errBrokenDecoder
	err = loader.Load()
	if !errors.Is(err, errBrokenDecoder) {
		t.Fatal(err)
	}

	// wrong value
	dec.values = map[string]string{"INT": "text"}
	dec.decodeErr = nil
	err = loader.Load()
	if !errors.Is(err, strconv.ErrSyntax) {
		t.Fatal(err)
	}
}
