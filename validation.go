package fconfig

import (
	"fmt"
	"strings"

	"gitlab.com/so_literate/fconfig/internal/model"
)

func (l *Loader) isFieldRequired(field *model.Field) bool {
	if l.Config.AllFieldsRequired {
		return true
	}

	reqValue, ok := field.GetTag(TagRequired)
	if !ok {
		return false
	}

	return reqValue != "false"
}

func isValueOneOfEnum(field *model.Field) (enumValues, value string, ok bool) {
	enumValues, ok = field.GetTag(TagEnum)
	if !ok {
		return "", "", true
	}

	value = fmt.Sprint(field.Value.Interface())
	enums := strings.Split(enumValues, ",") // `enum:"1,2,3"` or `enum:"[1 2],[3 4]"`

	for _, enum := range enums {
		if enum == value {
			return "", "", true
		}
	}

	return enumValues, value, false
}

func (l *Loader) checkFields() error {
	for _, field := range l.fields {
		if l.isFieldRequired(field) && !field.IsSet {
			return fmt.Errorf("%w: %s", ErrEmptyRequiredField, field.String())
		}

		enum, value, ok := isValueOneOfEnum(field)
		if !ok {
			return fmt.Errorf("%w: %s: enum %q, value %q", ErrNotEnumValue, field.String(), enum, value)
		}
	}

	return nil
}
