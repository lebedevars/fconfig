// Package json contains JSON config file decoder.
package json

import (
	"encoding/json"
	"fmt"
	"os"
)

// Decoder implements JSON FileDecoder in fconfig.
var Decoder = decoder{}

// decoder implements FileDecoder of the config.
type decoder struct{}

// Format returns ext of the supported file.
func (decoder) Formats() []string {
	return []string{".json"}
}

// DecodeFile reads and decodes file to config destination.
func (decoder) DecodeFile(path string, dst interface{}) error {
	file, err := os.Open(path) // nolint:gosec // it's ok with Potential file inclusion via variable.
	if err != nil {
		return fmt.Errorf("os.Open: %w", err)
	}

	defer file.Close() // nolint:errcheck,gosec // don't care about this error.

	if err = json.NewDecoder(file).Decode(dst); err != nil {
		return fmt.Errorf("json.Decode: %w", err)
	}

	return nil
}

// Marshal returns the JSON encoding of v.
func (decoder) Marshal(v interface{}) ([]byte, error) {
	return json.MarshalIndent(v, "", "  ") // nolint:wrapcheck // no need in an additional context.
}
