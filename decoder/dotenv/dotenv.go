// Package dotenv contains .env config file decoder.
package dotenv

import (
	"errors"
	"fmt"

	"github.com/joho/godotenv"
)

var (
	// ErrUnimplementedMarshal returns in Marshal method.
	ErrUnimplementedMarshal = errors.New("unimplemented marshal: use PrintEnvDefaults or PrintEnvK8SDefaults instead")
	// ErrWrongDestinationType returns when DecodeFile method got destination with wrong type.
	ErrWrongDestinationType = errors.New("destination is invalid")
)

// Decoder implements .env FileDecoder in fconfig.
var Decoder = decoder{}

// decoder implements FileDecoder of the config.
type decoder struct{}

// Format returns ext of the supported file.
func (decoder) Formats() []string {
	return []string{".env"}
}

// DecodeFile reads and decodes file to config destination.
func (decoder) DecodeFile(path string, dst interface{}) error {
	values, ok := dst.(*map[string]string)
	if !ok {
		return fmt.Errorf("%w: want '*map[string]string', got %v(%T)", ErrWrongDestinationType, dst, dst)
	}

	envs, err := godotenv.Read(path)
	if err != nil {
		return fmt.Errorf("failed to read file: %w", err)
	}

	*values = envs

	return nil
}

// Marshal returns unimplement error.
func (decoder) Marshal(v interface{}) ([]byte, error) {
	return nil, ErrUnimplementedMarshal
}
