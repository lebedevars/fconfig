package yaml_test

import (
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig/decoder/yaml"
)

func TestFormats(t *testing.T) {
	t.Parallel()

	formats := yaml.Decoder.Formats()
	if !reflect.DeepEqual(formats, []string{".yml", ".yaml"}) {
		t.Fatal(formats)
	}
}

type File struct {
	Int    int
	String string
	Slice  []int `yaml:"Slice"`
	Struct struct {
		Field string `yaml:"Field"`
	}
	Empty int `yaml:",omitempty"`
}

func TestDecodeFile(t *testing.T) {
	t.Parallel()

	got := File{}

	err := yaml.Decoder.DecodeFile("./testdata/file.yaml", &got)
	if err != nil {
		t.Fatal(err)
	}

	want := File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	want.Struct.Field = "sub field"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	// file not found
	err = yaml.Decoder.DecodeFile("./testdata/not_found.yaml", &got)
	if os.IsNotExist(err) {
		t.Fatal(err)
	}

	// broken dst file
	err = yaml.Decoder.DecodeFile("./testdata/broken.yaml", &got)
	if err == nil {
		t.Fatal("empty error on broken file")
	}
}

func TestMarshal(t *testing.T) {
	t.Parallel()

	val := &File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	val.Struct.Field = "sub field"

	got, err := yaml.Decoder.Marshal(val)
	if err != nil {
		t.Fatal(err)
	}

	want, err := ioutil.ReadFile("./testdata/file.yaml")
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(string(want), string(got)) {
		t.Fatalf("\nwant: %q\ngot:  %q", want, got)
	}
}
