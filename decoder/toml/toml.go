// Package toml contains TOML config file decoder.
package toml

import (
	"bytes"
	"fmt"
	"os"

	toml "github.com/pelletier/go-toml"
)

// Decoder implements TOML FileDecoder in fconfig.
var Decoder = decoder{}

// decoder implements FileDecoder of the config.
type decoder struct{}

// Format returns ext of the supported file.
func (decoder) Formats() []string {
	return []string{".toml"}
}

// DecodeFile reads and decodes file to config destination.
func (decoder) DecodeFile(path string, dst interface{}) error {
	file, err := os.Open(path) // nolint:gosec // it's ok with Potential file inclusion via variable.
	if err != nil {
		return fmt.Errorf("os.Open: %w", err)
	}

	defer file.Close() // nolint:errcheck,gosec // don't care about this error.

	if err = toml.NewDecoder(file).Decode(dst); err != nil {
		return fmt.Errorf("toml.Decode: %w", err)
	}

	return nil
}

// Marshal returns the TOML encoding of v.
func (decoder) Marshal(v interface{}) ([]byte, error) {
	buf := bytes.Buffer{}

	if err := toml.NewEncoder(&buf).Order(toml.OrderPreserve).Encode(v); err != nil {
		return nil, fmt.Errorf("toml.Encode: %w", err)
	}

	return buf.Bytes(), nil
}
