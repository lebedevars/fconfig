package fconfig_test

import (
	"errors"
	"testing"

	"gitlab.com/so_literate/fconfig"
)

func TestInit(t *testing.T) {
	t.Parallel()

	type dst struct {
		int int // nolint:unused,structcheck // it's used to test skipper of the unexported types.
	}

	config := &fconfig.Config{FlagArgs: []string{}}

	err := fconfig.New(new(dst), config).SkipDefaults().SkipEnv().SkipFiles().SkipFlags().Load()
	if !errors.Is(err, fconfig.ErrAllSourcesSkipped) {
		t.Fatal(err)
	}

	// bad dst value
	err = fconfig.New(dst{}).Load()
	if !errors.Is(err, fconfig.ErrInvalidConfigDestination) {
		t.Fatal(err)
	}

	// still bad value
	err = fconfig.New(&dst{}).Load()
	if !errors.Is(err, fconfig.ErrInvalidConfigDestination) {
		t.Fatal(err)
	}
}
