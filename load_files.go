package fconfig

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/so_literate/fconfig/internal/analyzer"
	"gitlab.com/so_literate/fconfig/internal/util"
)

// loadFileEnv special case for dotenv file.
func (l *Loader) loadFileEnv(file string, dec FileDecoder) error {
	var dst map[string]string

	if err := dec.DecodeFile(file, &dst); err != nil {
		return fmt.Errorf("failed to decode file: %w", err)
	}

	for _, field := range l.fields {
		value, ok := dst[l.getEnvName(field)]
		if !ok {
			continue
		}

		if err := util.SetFieldValue(field, value); err != nil {
			return fmt.Errorf("failed to set value into field: %s: %w", field.String(), err)
		}
	}

	return nil
}

func (l *Loader) loadFile(file string) error {
	if _, err := os.Stat(file); os.IsNotExist(err) {
		if l.Config.FileNotFoundAlert {
			return fmt.Errorf("file not found: %w", err)
		}

		return nil
	}

	format := filepath.Ext(file)

	dec, ok := l.Config.FileDecoders[format]
	if !ok {
		return ErrDecoderNotFound
	}

	if format == ".env" { // special case for .env files.
		return l.loadFileEnv(file, dec)
	}

	dst := util.AllocCopyValue(l.cleanDestination)

	if err := dec.DecodeFile(file, dst); err != nil {
		return fmt.Errorf("failed to decode file: %w", err)
	}

	secondary := analyzer.New(false).GetFields(dst)

	util.MergeFields(l.fields, secondary, l.Config.FileForceMerge)

	return nil
}

func (l *Loader) loadFiles() error {
	if l.Config.SkipFiles {
		return nil
	}

	if l.fileFlagConfig != nil && *l.fileFlagConfig != "" {
		l.Config.Files = append(l.Config.Files, *l.fileFlagConfig)
	}

	for _, file := range l.Config.Files {
		if err := l.loadFile(file); err != nil {
			return fmt.Errorf("%q: %w", file, err)
		}
	}

	return nil
}
